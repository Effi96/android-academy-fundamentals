package com.example.totaldailysales01

import java.time.LocalDateTime
import java.time.DayOfWeek

class OrdersAnalyzer {

    data class Order(
        val orderId: Int,
        val creationDate: LocalDateTime,
        val orderLines: List<OrderLine>
    )

    data class OrderLine(
        val productId: Int,
        val name: String,
        val quantity: Int,
        val unitPrice: Double
    )

    fun totalDailySales(orders: List<Order>): Map<DayOfWeek, Int> {
        val result: MutableMap<DayOfWeek, Int> = mutableMapOf<DayOfWeek, Int>()
        for (order in orders) {
            val orderCreationDateDayOfWeek = order.creationDate.dayOfWeek
            if (!result.containsKey(orderCreationDateDayOfWeek)) {
                result[orderCreationDateDayOfWeek] = 0
            }
            for (orderLine in order.orderLines) {
                result[orderCreationDateDayOfWeek] =
                    result[orderCreationDateDayOfWeek]!! + orderLine.quantity
            }
        }
        return result
    }
}

fun createOrderLine(
    productName: String,
    quantity: Int,
    unitPrice: Double
): OrdersAnalyzer.OrderLine {
    if (productName == "Pencil") {
        return OrdersAnalyzer.OrderLine(9872, "Pencil", quantity, unitPrice)
    } else if (productName == "Eraser") {
        return OrdersAnalyzer.OrderLine(1746, "Eraser", quantity, unitPrice)
    } else if (productName == "Pen") {
        return OrdersAnalyzer.OrderLine(5723, "Pen", quantity, unitPrice)
    } else if (productName == "Erasers Set") {
        return OrdersAnalyzer.OrderLine(3433, "Erasers Set", quantity, unitPrice)
    } else {
        return OrdersAnalyzer.OrderLine(4098, "Marker", quantity, unitPrice)
    }
}

fun main() {
    val orders: List<OrdersAnalyzer.Order> = listOf(
        OrdersAnalyzer.Order(
            554, LocalDateTime.of(2017, 3, 25, 10, 35, 20), listOf(
                createOrderLine("Pencil", 3, 3.00)
            )
        ),
        OrdersAnalyzer.Order(
            555, LocalDateTime.of(2017, 3, 25, 11, 24, 20), listOf(
                createOrderLine("Pencil", 2, 3.00),
                createOrderLine("Eraser", 1, 1.00)
            )
        ),
        OrdersAnalyzer.Order(
            453, LocalDateTime.of(2017, 3, 27, 14, 53, 12), listOf(
                createOrderLine("Pen", 4, 4.22),
                createOrderLine("Pencil", 3, 3.12),
                createOrderLine("Erasers Set", 1, 6.15)
            )
        ), OrdersAnalyzer.Order(
            431, LocalDateTime.of(2017, 3, 20, 12, 15, 2), listOf(
                createOrderLine("Pen", 7, 4.22),
                createOrderLine("Erasers Set", 2, 6.15)
            )
        ), OrdersAnalyzer.Order(
            690, LocalDateTime.of(2017, 3, 26, 11, 14, 0), listOf(
                createOrderLine("Pencil", 4, 3.12),
                createOrderLine("Marker", 5, 4.50)
            )
        )
    )

    val totalDailySales: Map<DayOfWeek, Int> = OrdersAnalyzer().totalDailySales(orders)

    for ((dayOfWeek, quantity) in totalDailySales) {
        println("$dayOfWeek: $quantity")
    }
}